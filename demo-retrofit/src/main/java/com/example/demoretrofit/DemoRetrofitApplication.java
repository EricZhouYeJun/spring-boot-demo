package com.example.demoretrofit;

import com.github.lianjiatech.retrofit.spring.boot.annotation.RetrofitScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RetrofitScan("com.example.demoretrofit.client")
@SpringBootApplication
public class DemoRetrofitApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoRetrofitApplication.class, args);
    }

}
