package com.example.demoretrofit.client;

import com.example.demoretrofit.entity.User;
import com.github.lianjiatech.retrofit.spring.boot.annotation.RetrofitClient;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

/**
 * @author zhouyejun
 * @Description TODO
 * @createTime 2022年02月11日
 */
@RetrofitClient(baseUrl = "http://192.168.10.23:8081")
public interface UserRetrogit {

    @GET("/user/getUserById/{userId}")
    User getUserById(@Path("userId") Integer user);


    @GET("/user/getUserList")
    List<User> getUserList();
}
