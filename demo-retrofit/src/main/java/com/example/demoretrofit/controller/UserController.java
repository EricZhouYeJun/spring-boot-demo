package com.example.demoretrofit.controller;

import com.example.demoretrofit.entity.User;
import com.example.demoretrofit.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhouyejun
 * @Description TODO
 * @createTime 2022年02月11日
 */
@Api(tags = "用户模块")
@RestController
@RequestMapping("/user")
public class UserController {


    @Autowired
    private UserService userService;


    @ApiOperation(value = "根据用户id查询用户信息", notes = "根据用户id查询用户信息")
    @GetMapping("/getUserById/{userId}")
    public User getUserById(@PathVariable Integer userId) {
        User user = new User();
        user.setUserId(1);
        user.setPassword("123456");
        user.setUserName("eric");
        user.setAge(18);
        if (user.getUserId().equals(userId)) {
            return user;
        }
        return null;
    }


    @ApiOperation(value = "查询用户信息列表", notes = "查询用户信息列表")
    @GetMapping("/getUserList")
    public List<User> getUserList() {
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setUserId(i);
            user.setPassword("123456" + i);
            user.setUserName("张" + i);
            user.setAge(18 + i);
            userList.add(user);
        }
        return userList;
    }


    @ApiOperation(value = "Retrofit测试getUserById", notes = "Retrofit测试getUserById")
    @GetMapping("/testGetUserById")
    public User testGetUserById() {
        User user = userService.getUserById(1);
        return user;
    }


    @ApiOperation(value = "Retrofit测试getUserList", notes = "Retrofit测试getUserList")
    @GetMapping("/testUserList")
    public List<User> testUserList() {
        List<User> userList = userService.getUserList();
        return userList;
    }
}
