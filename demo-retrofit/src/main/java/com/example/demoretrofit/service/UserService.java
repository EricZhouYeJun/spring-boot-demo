package com.example.demoretrofit.service;

import com.example.demoretrofit.entity.User;

import java.util.List;

/**
 * @author zhouyejun
 * @Description TODO
 * @createTime 2022年02月11日
 */

public interface UserService {

    User getUserById(Integer user);


    List<User> getUserList();
}
