package com.example.demoretrofit.service.impl;

import com.example.demoretrofit.client.UserRetrogit;
import com.example.demoretrofit.entity.User;
import com.example.demoretrofit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhouyejun
 * @Description TODO
 * @createTime 2022年02月11日
 */
@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRetrogit userRetrogit;

    @Override
    public User getUserById(Integer userId) {
        User user = userRetrogit.getUserById(userId);
        return user;
    }

    @Override
    public List<User> getUserList() {
        List<User> userList = userRetrogit.getUserList();
        return userList;
    }
}
