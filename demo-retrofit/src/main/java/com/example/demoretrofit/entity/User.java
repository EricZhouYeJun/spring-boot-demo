package com.example.demoretrofit.entity;

import lombok.Data;

/**
 * @author zhouyejun
 * @Description TODO
 * @createTime 2022年02月11日
 */
@Data
public class User {

    private  Integer userId;

    private  String userName;

    private  String password;

    private  Integer age;

}
