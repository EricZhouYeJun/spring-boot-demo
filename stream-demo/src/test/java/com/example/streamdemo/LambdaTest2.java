package com.example.streamdemo;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;

public class LambdaTest2 {

	class User {
		public String name;
		public int score;

        public User() {

        }


        @Override
		public String toString() {
			return "User [name=" + name + ", score=" + score + "]";
		}

		public User(String name, int score) {
			this.name = name;
			this.score = score;
		}

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }
    }

	@Test
	public void testOldUse1() {
        User user = new User();
        user.setName("eric");
        user.setScore(18);
	}

}
