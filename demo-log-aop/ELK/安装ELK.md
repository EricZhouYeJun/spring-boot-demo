> 
>
> 前言:ELK 即 Elasticsearch、Logstash、Kibana,组合起来可以搭建线上日志系统，在目前这种分布式微服务系统中，通过 ELK 会非常方便的查询和统计日志情况。

# 1.ELK 中各个服务的作用

- Elasticsearch:用于存储收集到的日志信息；
- Logstash:用于收集日志，应用整合了 Logstash 以后会把日志发送给 Logstash,Logstash 再把日志转发给 Elasticsearch；
- Kibana:通过 Web 端的可视化界面来查看日志。

# 2.使用 Docker Compose 搭建 ELK 环境

编写 docker-compose.yml 脚本启动 ELK 服务

```bash
version: '3'
services:
  elasticsearch:
    image: elasticsearch:7.2.0
    container_name: elasticsearch
    environment:
      - "cluster.name=elasticsearch" #设置集群名称为elasticsearch
      - "discovery.type=single-node" #以单一节点模式启动
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m" #设置使用jvm内存大小
    volumes:
      - /mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins #插件文件挂载
      - /mydata/elasticsearch/data:/usr/share/elasticsearch/data #数据文件挂载
    ports:
      - 9200:9200
  kibana:
    image: kibana:7.2.0
    container_name: kibana
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    environment:
      - "elasticsearch.hosts=http://es:9200" #设置访问elasticsearch的地址
    ports:
      - 5601:5601
  logstash:
    image: logstash:7.2.0
    container_name: logstash
    volumes:
      - /mydata/logstash/upms-logstash.conf:/usr/share/logstash/pipeline/logstash.conf
    depends_on:
      - elasticsearch #kibana在elasticsearch启动之后再启动
    links:
      - elasticsearch:es #可以用es这个域名访问elasticsearch服务
    ports:
      - 4560:4560
```

# 3.创建对应容器挂载目录

```bash
mkdir -p /mydata/logstash

mkdir -p /mydata/elasticsearch/data

mkdir -p /mydata/elasticsearch/plugins


chmod 777 /mydata/elasticsearch/data  # 给777权限，不然启动elasticsearch 可能会有权限问题
```

# 4.编写日志采集 logstash

在 `/mydata/logstash`目录创建 `upms-logstash.conf`

```bash
input {
  tcp {
    mode => "server"
    host => "0.0.0.0"
    port => 4560
    codec => json_lines
  }
}
output {
  elasticsearch {
    hosts => "es:9200"
    index => "upms-logstash-%{+YYYY.MM.dd}"
  }
}
```

# 5.启动 ELK 服务

在`docker-compose.yml` 同级目录执行 `docker-compose up -d`

注意：Elasticsearch 启动可能需要好几分钟，要耐心等待。

![img](%E5%AE%89%E8%A3%85ELK.assets/20200915165205.png)

# 6.logstash 安装 json_lines 格式插件

```bash
# 进入logstash容器
docker exec -it logstash /bin/bash
# 进入bin目录
cd /bin/
# 安装插件
logstash-plugin install logstash-codec-json_lines
# 退出容器
exit
# 重启logstash服务
docker restart logstashCopy to clipboardErrorCopied
```

# 7.SpringBoot整合 Logstash

## 添加 pom 依赖

```java
<!--集成logstash-->
<dependency>
    <groupId>net.logstash.logback</groupId>
    <artifactId>logstash-logback-encoder</artifactId>
    <version>5.3</version>
</dependency>Copy to clipboardErrorCopied
```

## logback-spring.xml 新增 appender

```bash
 <!--输出到logstash的appender-->
<appender name="LOGSTASH" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
    <!--可以访问的logstash日志收集端口-->
    <destination>192.168.0.31:4560</destination>
    <encoder charset="UTF-8" class="net.logstash.logback.encoder.LogstashEncoder"/>
</appender>
<root level="INFO">
    <appender-ref ref="LOGSTASH"/>
</root>Copy to clipboardErrorCopied
```



<font color="red">扩展:将logstash的ip端口进行配置化，这样以后更改ip地址就可以在配置中心直接改了，不需要在logback-spring.xml文件中更改，以免需要重启服务，很不方便。</font>

![image-20220207172111582](%E5%AE%89%E8%A3%85ELK.assets/image-20220207172111582.png)



![image-20220207172540395](%E5%AE%89%E8%A3%85ELK.assets/image-20220207172540395.png)

```bash
<!--读取application.yml文件中的变量logstash.ip   name可以随便取-->
<springProperty scope="context" name="logstashIp" source="logstash.ip"/>

  <!--输出到logstash的appender-->
  <appender name="LOGSTASH" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
    <!--${logstashIp}这样就可以直接引用application的变量了-->
    <destination>${logstashIp}</destination>
    <encoder charset="UTF-8" class="net.logstash.logback.encoder.LogstashEncoder"/>
  </appender>

```



# 8. 在 kibana 中查询日志

在浏览器输入：ip:5601 进入kibana控制台

![img](%E5%AE%89%E8%A3%85ELK.assets/20200915165221.png)

 ![img](%E5%AE%89%E8%A3%85ELK.assets/20200915165230.png)

# 9.同时采集多个服务的日志

```bash
每个应用发送到不同的TCP 端口 这里注意logstash 的容器端口映射增加
input {
  tcp {
   add_field => {"service" => "upms"}
    mode => "server"
    host => "0.0.0.0"
    port => 4560
    codec => json_lines
  }
  tcp {
   add_field => {"service" => "auth"}
    mode => "server"
    host => "0.0.0.0"
    port => 4561
    codec => json_lines
  }
}
output {
   if [service] == "upms"{
    elasticsearch {
      hosts => "es:9200"
      index => "upms-logstash-%{+YYYY.MM.dd}"
    }
   }
  if [service] == "auth"{
    elasticsearch {
      hosts => "es:9200"
      index => "auth-logstash-%{+YYYY.MM.dd}"
    }
   }
}
```



# 10.kibana 设置中文

在config/kibana.yml尾部添加一行配置即可(7.0版本后)。

```bash
//冒号后必须加空格
i18n.locale: "zh-CN"
```

# 11.logstash与本地时间相差 8个小时

解决logstash 7.x 中时间问题，@timestamp 与本地时间相差 8个小时

找到配置文件 输入以下这段代码就行

![image-20220126164352430](%E5%AE%89%E8%A3%85ELK.assets/image-20220126164352430.png)

```bash
filter {
	
	ruby { 
		code => "event.set('timestamp', event.get('@timestamp').time.localtime + 8*60*60)" 
	}	
	
	ruby {
		code => "event.set('@timestamp',event.get('timestamp'))"
	}
	
	mutate {
		remove_field => ["timestamp"]
	}
}
```



# 12.kibana设置时间格式

在kibana控制台设置中设置时间格式

![image-20220207144833096](%E5%AE%89%E8%A3%85ELK.assets/image-20220207144833096.png)



![image-20220207144904679](%E5%AE%89%E8%A3%85ELK.assets/image-20220207144904679.png)



# 13.Elastic,Kibana,Logstash设置密码

[参考网址]([docker容器Elastic7.8+Kibana7.8设置密码 - 简书 (jianshu.com)](https://www.jianshu.com/p/ed46bbc17a3f))

docker容器Elastic7.2+Kibana7.2设置密码

## ElasticSearch7.2配置

```bash
#进入es容器内部
docker exec -it es /bin/bash
#修改es配置文件elasticsearch.yml
vi /usr/share/elasticsearch/config/elasticsearch.yml
#添加以下内容
xpack.security.enabled: true
xpack.license.self_generated.type: basic
xpack.security.transport.ssl.enabled: true
#保存后退出docker容器
exit
#重启es
docker restart es
#重启后进入es容器中
docker exec -it es /bin/bash
#进入es 名录目录 
cd /usr/share/elastic/bin
#执行命令，交互式设置密码（注意保存好全部密码）
./elasticsearch-setup-passwords interactive
```

## Kibana7.2配置

```bash
#进入es容器内部
docker exec -it kibana /bin/bash
#修改es配置文件kibana.yml
vi /usr/share/kibana/config/kibana.yml
#添加以下内容
elasticsearch.username: "elastic"
elasticsearch.password: "刚刚elastic设置的密码"
#保存后退出docker容器
exit
#重启kibana
docker restart kibana
```



## logstash7.2配置

[参考网址]([Elasticsearch、Logstash、Kibana手把手教你添加密码设置 - 简书 (jianshu.com)](https://www.jianshu.com/p/4aa3a8b70bfa))

```bash
#进入logstash容器内部
docker exec -it logstash /bin/bash
#修改logstash配置文件logstash.yml
vi config/logstash.yml
#添加以下内容
xpack.monitoring.enabled: true
xpack.monitoring.elasticsearch.username: "elastic"
xpack.monitoring.elasticsearch.password: "刚刚elastic设置的密码"
xpack.monitoring.elasticsearch.hosts: ["http://elastic的IP地址:9200"]
#保存后退出docker容器
exit
#重启logstash
docker restart logstash
```

在logstash指定的启动文件中加上具有索引权限的账号（登录kibana后创建的用户）

```bash

output {
  elasticsearch {
    hosts => "es:9200"
    index => "wonlyapp-logstash-%{+YYYY.MM.dd}"
    user => "elastic"
    password => "admin123456"
  }
}

```

